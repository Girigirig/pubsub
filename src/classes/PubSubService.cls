/**
 * @description A simple Pub/Sub pattern implementation
 */
public with sharing class PubSubService {
    private PubSubService() { }
    private List<IncomingLeadHandler> observers = new List<IncomingLeadHandler>();
    /**
     * @description A singleton for service interaction.
     */
    public static PubSubService Instance {
        get {
            if (Instance == null) {
                Instance = new PubSubService();
            }

            return Instance;
        }

        private set;
    }

    /**
     * @description Subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
     * @throws ArgumentNullException if implementation is null.
     */
    public void subscribe(IHandleMessages implementation) {
        if(implementation == null) {
            return;
        }
        IncomingLeadHandler handler = (IncomingLeadHandler)implementation;
        observers.add(handler);
    }

    /**
     * @description Un-subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
     * @throws ArgumentNullException if implementation is null.
     */
    public void unsubscribe(IHandleMessages implementation) {
        Integer x = 0;
        for(IHandleMessages h : observers) {
            if( h == implementation) {
                observers.remove(x);
                return;
            }
            x++;

        }
    }

    /**
     * @description Emits a message to a given channel containing the specified data.
     * @param channel The channel to emit a message on.
     * @param data The data to emit.
     * @throws ArgumentNullException if channel is null.
     */
    public void emit(String channel, Object data){
        Lead lead = (Lead)data;
        if(lead.FirstName == null) {
            data = null;
        }
        if(channel != 'IncomingLeads' || data == null) {
            channel = 'error';
            ArgumentNullException.throwIfNull(data, channel );
        }

        if(channel == 'IncomingLeads' && (lead.FirstName != null || lead.FirstName != '' || data != null ) && !observers.isEmpty() ) {
            insert lead;
        }

        IncomingLeadHandler.channels.add(channel);
        for(IncomingLeadHandler o : observers) {
            o.handleMessage(channel, data);
        }
    }
}